package com.correios.principal;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import org.apache.axis.AxisFault;
import org.tempuri.CResultado;
import org.tempuri.CalcPrecoPrazoWSLocator;
import org.tempuri.CalcPrecoPrazoWSSoapStub;

public class Principal {
	
	
	public static void main(String[] args){
			
		
		try {
			CResultado resultado = new CResultado();
			CResultado resultado2 = new CResultado();
			
			CalcPrecoPrazoWSLocator loc = new CalcPrecoPrazoWSLocator();
			CalcPrecoPrazoWSSoapStub stub = new CalcPrecoPrazoWSSoapStub(new URL(loc.getCalcPrecoPrazoWSSoapAddress()), null);
			
			resultado = stub.calcPrazo("40010", "60821802","60060160");
			resultado2 = stub.calcPrecoFAC("40010", "10", "14/06/2017");
			
			System.out.println("Dias para entregar: "+resultado.getServicos()[0].getPrazoEntrega());
			System.out.println("Entrega em Domicilio: " + resultado.getServicos()[0].getEntregaDomiciliar());
			System.out.println("Entrega no Sabado: "+ resultado.getServicos()[0].getEntregaSabado());
			System.out.println("Valor da entrega:" + resultado2.getServicos()[0].getValor());
			
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
